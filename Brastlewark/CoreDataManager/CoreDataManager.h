//
//  CoreDataManager.h
//  Brastlewark
//
//  Created by Aleix Cos Pous on 4/2/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Gnome.h"
#import "AppDelegate.h"

@interface CoreDataManager : NSObject
+(CoreDataManager *)sharedManager;
@property (nonatomic, strong) AppDelegate *appDelegate;
@property (nonatomic, strong) NSManagedObjectContext *managedContext;
-(void)saveGnomeFromDict:(NSDictionary*)gnomeDictionary completionHandler:(void (^)(NSError* error))handler;
-(void)fetchGnomesWithCompletionHandler:(void (^)(NSArray* gnomes, NSError* error))handler;
-(void)fetchGnomeByName:(NSString*)gnomeName completionHandler:(void (^)(Gnome* gnome, NSError* error))handler;
@end
