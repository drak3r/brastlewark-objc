//
//  CoreDataManager.m
//  Brastlewark
//
//  Created by Aleix Cos Pous on 4/2/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

#import "CoreDataManager.h"
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

static CoreDataManager *_sharedCoreDataManager;

@implementation CoreDataManager

+(CoreDataManager*)sharedManager{
    if(!_sharedCoreDataManager){
        _sharedCoreDataManager = [[CoreDataManager alloc] init];
        _sharedCoreDataManager.appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        _sharedCoreDataManager.managedContext = _sharedCoreDataManager.appDelegate.managedObjectContext;
    }
    return _sharedCoreDataManager;

}
-(void)saveGnomeFromDict:(NSDictionary*)gnomeDictionary completionHandler:(void (^)(NSError *))handler{
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"GnomeEntity" inManagedObjectContext:_sharedCoreDataManager.managedContext];
    Gnome *gnome = (Gnome*)[[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:_sharedCoreDataManager.managedContext];
    
    if(gnomeDictionary[@"id"]){
        gnome.gnomeId = [gnomeDictionary[@"id"] intValue];
    }
    if(gnomeDictionary[@"name"]){
        gnome.name = gnomeDictionary[@"name"];
    }
    if(gnomeDictionary[@"thumbnail"]){
        gnome.thumbnailURL = gnomeDictionary[@"thumbnail"];
    }
    if(gnomeDictionary[@"age"]){
        gnome.age = [gnomeDictionary[@"age"] intValue];
    }
    if(gnomeDictionary[@"weight"]){
        gnome.weight = [gnomeDictionary[@"weight"] doubleValue];
    }
    if(gnomeDictionary[@"height"]){
        gnome.height = [gnomeDictionary[@"height"] doubleValue];
    }
    if(gnomeDictionary[@"hair_color"]){
        NSString *hairColor = gnomeDictionary[@"hair_color"];
        gnome.hairColor = hairColor;
        if([hairColor isEqual:@"Pink"]){
            gnome.gender = @"Female";
        } else {
            gnome.gender = @"Male";
        }
    }
    if(gnomeDictionary[@"professions"]){
        gnome.professions = gnomeDictionary[@"professions"];
    }
    if(gnomeDictionary[@"friends"]){
        gnome.friends = gnomeDictionary[@"friends"];
    }
    
    NSError *error = nil;
    [_sharedCoreDataManager.managedContext save:&error];
    
    if(error){
        NSLog(@"Could not save %@", error);
        handler(error);
        return;
    }
    handler(nil);
}

-(void)fetchGnomesWithCompletionHandler:(void (^)(NSArray *, NSError *))handler{
    NSFetchRequest *fetchRequest = [NSFetchRequest<NSManagedObject *> fetchRequestWithEntityName:@"GnomeEntity"];
    NSError *error = nil;
    NSArray *gnomes = [_sharedCoreDataManager.managedContext executeFetchRequest:fetchRequest error:&error];
    if(error){
        NSLog(@"Could not fetch %@", error);
        handler(nil, error);
        return;
    }
    handler(gnomes, nil);
}
-(void)fetchGnomeByName:(NSString *)gnomeName completionHandler:(void (^)(Gnome *, NSError *))handler{
    NSFetchRequest *fetchRequest = [NSFetchRequest<NSManagedObject *> fetchRequestWithEntityName:@"GnomeEntity"];
    NSError *error = nil;

    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"name == %@", gnomeName];
    if(error){
        NSLog(@"Could not fetch %@", error);
        handler(nil, error);
        return;
    }
    NSArray *gnomes = [_sharedCoreDataManager.managedContext executeFetchRequest:fetchRequest error:&error];
    handler(gnomes.firstObject, nil);
}

@end
