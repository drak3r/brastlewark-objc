//
//  Gnome.m
//  Brastlewark
//
//  Created by Aleix Cos Pous on 4/2/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

#import "Gnome.h"

@implementation Gnome
@dynamic gnomeId;
@dynamic name;
@dynamic thumbnailURL;
@dynamic age;
@dynamic weight;
@dynamic height;
@dynamic hairColor;
@dynamic friends;
@dynamic professions;
@dynamic gender;

-(UIColor *)getHairGnomeColor{
    if ([self.hairColor isEqual:@"Red"]) {
        return [UIColor redColor];
    } else if ([self.hairColor isEqual:@"Gray"]){
        return [UIColor grayColor];
    }else if ([self.hairColor isEqual:@"Pink"]){
        return [UIColor colorWithRed:1 green:192/255.0 blue:203/255.0 alpha:1];
    }else if ([self.hairColor isEqual:@"Black"]){
        return [UIColor blackColor];
    }else{
        return [UIColor greenColor];
    }
}
@end
