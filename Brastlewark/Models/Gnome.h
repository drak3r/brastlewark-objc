//
//  Gnome.h
//  Brastlewark
//
//  Created by Aleix Cos Pous on 4/2/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>

@interface Gnome : NSManagedObject
@property (nonatomic) int64_t gnomeId;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *thumbnailURL;
@property (nonatomic) int64_t age;
@property (nonatomic) double weight;
@property (nonatomic) double height;
@property (nonatomic, copy) NSString *hairColor;
@property (nonatomic, retain) NSArray *friends;
@property (nonatomic, retain) NSArray *professions;
@property (nonatomic, copy) NSString *gender;

-(UIColor*)getHairGnomeColor;

@end
