//
//  FilterProfessionsTableViewController.h
//  Brastlewark
//
//  Created by Aleix Cos Pous on 4/2/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FilterProfessionsDelegate <NSObject>
-(void)didSelectProfession:(NSString*)profession;
@end

@interface FilterProfessionsTableViewController : UITableViewController
@property (nonatomic, weak) id<FilterProfessionsDelegate> delegate;
@property (nonatomic, strong) NSString *selectedProfession;

@end
