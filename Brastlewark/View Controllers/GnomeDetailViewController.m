//
//  GnomeDetailViewController.m
//  Brastlewark
//
//  Created by Aleix Cos Pous on 4/2/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

#import "GnomeDetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "GnomeDetailCollectionViewCell.h"
#import "CoreDataManager.h"
#import "ShowAlert.h"

@interface GnomeDetailViewController ()<UICollectionViewDelegate, UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UILabel *labelAge;
@property (weak, nonatomic) IBOutlet UILabel *labelWeight;
@property (weak, nonatomic) IBOutlet UILabel *labelHeight;
@property (weak, nonatomic) IBOutlet UILabel *labelProfessions;
@property (weak, nonatomic) IBOutlet UILabel *labelFriendsOfGnome;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIImageView *gnomeImageView;
@property (weak, nonatomic) IBOutlet UIView *viewHairColor;
@property (weak, nonatomic) IBOutlet UILabel *labelGender;

@end

@implementation GnomeDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self configureView];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(animateImage)];
    tapGesture.numberOfTapsRequired = 1;
    _gnomeImageView.userInteractionEnabled = YES;
    [_gnomeImageView addGestureRecognizer:tapGesture];
}
#pragma mark - UI

-(void)viewDidLayoutSubviews{
    _viewHairColor.layer.cornerRadius = _viewHairColor.bounds.size.width/2.0;
    _viewHairColor.layer.masksToBounds = YES;
    _gnomeImageView.layer.cornerRadius = _gnomeImageView.bounds.size.width/2.0;
    _gnomeImageView.layer.masksToBounds = YES;
}
-(void)animateImage{
    CABasicAnimation *rotation = [[CABasicAnimation alloc] init];
    rotation.keyPath = @"transform.rotation.z";
    rotation.toValue = [[NSNumber alloc] initWithDouble: M_PI * 2];
    rotation.duration = 1;
    [_gnomeImageView.layer addAnimation:rotation forKey:@"rotationAnimation"];
}
-(void)configureView{
    if (_gnome){
        self.title = _gnome.name;
        _labelName.text = _gnome.name;
        _labelFriendsOfGnome.text = _gnome.name;
        _labelAge.text = [NSString stringWithFormat:@"%lli", _gnome.age];
        _labelWeight.text = [NSString stringWithFormat:@"%f", _gnome.weight];
        _labelHeight.text = [NSString stringWithFormat:@"%f", _gnome.height];
        _viewHairColor.backgroundColor = [_gnome getHairGnomeColor];
        _labelGender.text = _gnome.gender;
        if(_gnome.professions.count == 0){
            _labelProfessions.text = @"Doesn't work at the moment";
        }else {
            NSString *professions = @"";
            for (int i = 0; i < _gnome.professions.count; i++) {
                NSString *profession = _gnome.professions[i];
                if (i == 0) {
                    professions = profession;
                } else {
                    professions = [professions stringByAppendingString:[NSString stringWithFormat:@", %@", profession]];
                }
            }
        _labelProfessions.text = professions;
        }
        NSURL *URL = [NSURL URLWithString:_gnome.thumbnailURL];
        [_gnomeImageView sd_setImageWithURL:URL placeholderImage:[UIImage imageNamed:@"gnome"]];
    }
}
#pragma mark - Collection View Data Source/Delegate

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    if (_gnome.friends.count == 0){
        // Gnome has no friends. Show a label
        UILabel *labelNoFriends = [[UILabel alloc] initWithFrame:collectionView.bounds];
        labelNoFriends.text = [NSString stringWithFormat:@"%@ has no friends", _gnome.name];
        labelNoFriends.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:20.0];
        labelNoFriends.textAlignment = NSTextAlignmentCenter;
        [labelNoFriends adjustsFontSizeToFitWidth];
        collectionView.backgroundView = labelNoFriends;
        return 0;
    }else {
        collectionView.backgroundView = nil;
        return 1;
    }
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _gnome.friends.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    GnomeDetailCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"gnomeDetailCell" forIndexPath:indexPath];
    NSString *gnomeFriendName = _gnome.friends[indexPath.item];
    [[CoreDataManager sharedManager] fetchGnomeByName:gnomeFriendName completionHandler:^(Gnome *friend, NSError *error) {
        if (error){
            [ShowAlert alertWithTitle:nil message:error.description toViewController:self];
        } else {
            [cell.gnomeImageView sd_setImageWithURL:[NSURL URLWithString:friend.thumbnailURL]];
        }
    }];
    NSString *formattedFriendName = [gnomeFriendName componentsSeparatedByString:@" "].firstObject;
    cell.labelName.text = formattedFriendName;
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *gnomeFriendName = _gnome.friends[indexPath.item];
    [[CoreDataManager sharedManager] fetchGnomeByName:gnomeFriendName completionHandler:^(Gnome *friend, NSError *error) {
        if (error){
            [ShowAlert alertWithTitle:nil message:error.description toViewController:self];
        } else{
            GnomeDetailViewController *detailVC = (GnomeDetailViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"GnomeDetailViewController"];
            detailVC.gnome = friend;
            [self replaceTopViewControllerWith:detailVC animated:YES];
        }
    }];
}
#pragma mark - Navigation

-(void)replaceTopViewControllerWith:(UIViewController*)viewController animated:(BOOL)animated{
    NSMutableArray *vcs = [NSMutableArray arrayWithArray: self.navigationController.viewControllers];
    vcs[vcs.count - 1] = viewController;
    [self.navigationController setViewControllers:vcs animated:animated];
}
@end

