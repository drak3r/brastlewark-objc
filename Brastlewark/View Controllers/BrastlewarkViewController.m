//
//  BrastlewarkViewController.m
//  Brastlewark
//
//  Created by Aleix Cos Pous on 4/2/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

#import "BrastlewarkViewController.h"
#import "GnomeTableViewCell.h"
#import "NetworkOperation.h"
#import "Gnome.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CoreDataManager.h"
#import "FilterProfessionsTableViewController.h"
#import "GnomeDetailViewController.h"
#import "ShowAlert.h"

@interface BrastlewarkViewController () <UISearchResultsUpdating, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate, FilterProfessionsDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *labelDownloadingGnomes;

@property UISearchController *searchController;
@property NSArray *arrayOfGnomes;
@property NSArray *filteredGnomes;
@property NSString *filteredProfession;

@end

@implementation BrastlewarkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Brastlewark";
    _tableView.tableFooterView = [[UIView alloc] init];
    _searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    _searchController.searchResultsUpdater = self;
    _searchController.searchBar.delegate = self;
    _searchController.searchBar.scopeButtonTitles = @[@"Name", @"Hair Color"];
    _searchController.dimsBackgroundDuringPresentation = false;
    self.definesPresentationContext = true;
    _searchController.searchBar.barTintColor = [UIColor whiteColor];
    _tableView.tableHeaderView = _searchController.searchBar;
    [self getGnomesFromCoreData];
   
}

#pragma mark - Core Data & Network Methods

-(void)getGnomesFromCoreData{
    [[CoreDataManager sharedManager] fetchGnomesWithCompletionHandler:^(NSArray *gnomes, NSError *error) {
        if(error){
            [ShowAlert alertWithTitle:nil message:error.localizedDescription toViewController:self];
        } else {
            if(gnomes.count == 0){
                [self requestGnomes];
            } else {
                _arrayOfGnomes = gnomes;
                [self.tableView reloadData];
            }
        }
    }];
}

-(void)requestGnomes{
    [self hideViews:NO];
    [NetworkOperation requestGnomesWithResponseHandler:^(NSArray *gnomes, NSError *error) {
        if (gnomes){
            _arrayOfGnomes = gnomes;
            [self.tableView reloadData];
            [self hideViews:YES];
        } else {
            [ShowAlert alertWithTitle:nil message:error.localizedDescription toViewController:self];
            [self.activityIndicator stopAnimating];
            self.labelDownloadingGnomes.text = @"There was an error downloading the data";
        }
    }];
}
#pragma mark - UI
-(void)hideViews:(BOOL)hide{
    _labelDownloadingGnomes.hidden = hide;
    self.navigationItem.leftBarButtonItem.enabled = hide;
    if (hide){
        [_activityIndicator stopAnimating];
    } else {
        [_activityIndicator startAnimating];
    }
}
#pragma mark - Filter Methods

-(void)filterContentForText:(NSString*)text scope:(NSString*)scope{
    NSPredicate *predicate;
    if (text.length != 0){
        if([scope isEqual:@"Hair Color"]){
            if (_filteredProfession){
                // Put in the filtered array gnomes of profession selected and hair color the user is searching for
                predicate = [NSPredicate predicateWithFormat:@"hairColor contains[cd] %@ && professions contains[cd] %@", text, _filteredProfession];
            }else {
                // Put in the filtered array gnomes of the hair color desired
                predicate = [NSPredicate predicateWithFormat:@"hairColor CONTAINS[cd] %@", text.lowercaseString];
            }
        } else {
            if (_filteredProfession){
                // Put in the filtered array gnomes that has the name the user is searching and filtered by profession
                predicate = [NSPredicate predicateWithFormat:@"name contains[cd] %@ && professions contains[cd] %@", text, _filteredProfession];
            } else {
                // Put in the filtered array gnomes that has the name
                predicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", text.lowercaseString];
            }
        }
        _filteredGnomes = [_arrayOfGnomes filteredArrayUsingPredicate:predicate];
    }
    [_tableView reloadData];
}

-(void)filterContentForProfession:(NSString*)profession{
    if (![profession isEqual:@"All"]){
        _filteredGnomes = [_arrayOfGnomes filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"professions CONTAINS[cd] %@", profession]];
    }
    [self.tableView reloadData];

}
#pragma mark - Table View Data Source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ((_searchController.isActive && ![_searchController.searchBar.text  isEqual: @""]) || _filteredProfession ){
        return _filteredGnomes.count;
    }
    return _arrayOfGnomes.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GnomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"gnomeCell" forIndexPath:indexPath];
    Gnome* gnome;
    if ((_searchController.isActive && ![_searchController.searchBar.text  isEqual: @""]) || _filteredProfession)  {
        gnome = _filteredGnomes[indexPath.row];
    } else {
        gnome = _arrayOfGnomes[indexPath.row];
    }
    cell.labelName.text = gnome.name;
    cell.labelAge.text = [NSString stringWithFormat:@"%lli", gnome.age];
    
    if (gnome.professions.count == 0){
        cell.labelProfession.text = @"Doesn't work";
    } else {
        NSString* professions = @"";
        for (int i = 0; i < gnome.professions.count; i++) {
            NSString *profession = gnome.professions[i];
            if (i == 0) {
                professions = profession;
            } else {
                professions = [professions stringByAppendingString:[NSString stringWithFormat:@", %@", profession]];
            }
        }
        cell.labelProfession.text = professions;
    }
    
    NSURL *URL = [NSURL URLWithString:gnome.thumbnailURL];
    [cell.imageGnome sd_setImageWithURL:URL placeholderImage:[UIImage imageNamed:@"gnome"]];
    cell.viewHairColor.backgroundColor = [gnome getHairGnomeColor];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}
#pragma mark - Search Results & Search Bar Delegate

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    UISearchBar *searchBar = searchController.searchBar;
    NSString *scope = searchBar.scopeButtonTitles[searchBar.selectedScopeButtonIndex];
    [self filterContentForText:searchBar.text scope:scope];
}

-(void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope{
    [self filterContentForText:searchBar.text scope:searchBar.scopeButtonTitles[selectedScope]];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqual:@"showDetail"]){
        NSIndexPath *indexPath = self.tableView.indexPathForSelectedRow;
        Gnome* gnome;
        if ((_searchController.isActive && ![_searchController.searchBar.text  isEqual: @""]) || self.filteredProfession ){
            gnome = _filteredGnomes[indexPath.row];
        } else {
            gnome = _arrayOfGnomes[indexPath.row];
        }
        GnomeDetailViewController* detailVC = (GnomeDetailViewController*)segue.destinationViewController;
        detailVC.gnome = gnome;
    } else if ([segue.identifier isEqual:@"filterProfessions"]){
        FilterProfessionsTableViewController* filterProfessionsVC = (FilterProfessionsTableViewController*)((UINavigationController*)segue.destinationViewController).topViewController;
        filterProfessionsVC.delegate = self;
        if(_filteredProfession){
            filterProfessionsVC.selectedProfession = _filteredProfession;
        }
    }
}

#pragma mark - Filter Delegate

-(void)didSelectProfession:(NSString *)profession{
    if([profession isEqual:@"All"]){
        self.filteredProfession = nil;
    } else {
        self.filteredProfession = profession;
    }
    [self filterContentForProfession: profession];
}

@end
