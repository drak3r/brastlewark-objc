//
//  FilterProfessionsTableViewController.m
//  Brastlewark
//
//  Created by Aleix Cos Pous on 4/2/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

#import "FilterProfessionsTableViewController.h"
#import "CoreDataManager.h"
#import "Gnome.h"
#import "ProfessionTableViewCell.h"
#import "ShowAlert.h"

@interface FilterProfessionsTableViewController ()
@property (nonatomic, strong) NSMutableArray *professions;
@end

@implementation FilterProfessionsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getAllTheProfessions];
}

-(void)getAllTheProfessions{
    // Fetch gnoms and all the professions
    NSMutableArray *arrayProfessions = [NSMutableArray array];
    [[CoreDataManager sharedManager] fetchGnomesWithCompletionHandler:^(NSArray *gnomes, NSError *error) {
        if (error){
            [ShowAlert alertWithTitle:nil message:error.description toViewController:self];
        } else {
            for (Gnome* gnome in gnomes){
                for (NSString* profession in gnome.professions){
                    NSString* formattedProfession = [profession stringByReplacingOccurrencesOfString:@" " withString:@""];
                    if(![arrayProfessions containsObject:formattedProfession]){
                        [arrayProfessions addObject:formattedProfession];
                    }else {
                        break;
                    }
                }
            }
            _professions =[NSMutableArray arrayWithArray: [arrayProfessions sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]];
            [_professions insertObject:@"All" atIndex:0];
            [self.tableView reloadData];
        }
    } ];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source & delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _professions.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ProfessionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"professionCell" forIndexPath:indexPath];
    
    NSString *profession = _professions[indexPath.row];
    cell.labelProfession.text = profession;
    cell.labelProfession.textColor = [UIColor blackColor];
    cell.checkIcon.hidden = true;
    
    if (_selectedProfession){
        if([_selectedProfession isEqual:profession]){
            cell.labelProfession.textColor = [UIColor colorWithRed:1 green:0 blue:98/255.0 alpha:1];
            cell.checkIcon.hidden = false;
        }
    } else if (indexPath.row == 0){
        cell.labelProfession.textColor = [UIColor colorWithRed:1 green:0 blue:98/255.0 alpha:1];
        cell.checkIcon.hidden = false;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *profession = _professions[indexPath.row];
    if([profession isEqualToString:@"Tinker"]){
        profession = @" Tinker";
    }
    [_delegate didSelectProfession:profession];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)dismiss:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];

}



@end
