//
//  GnomeDetailViewController.h
//  Brastlewark
//
//  Created by Aleix Cos Pous on 4/2/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Gnome.h"

@interface GnomeDetailViewController : UIViewController
@property (nonatomic, strong) Gnome *gnome;
@end
