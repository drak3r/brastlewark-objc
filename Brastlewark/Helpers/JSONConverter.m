//
//  JSONConverter.m
//  Brastlewark
//
//  Created by Aleix Cos Pous on 4/2/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

#import "JSONConverter.h"

@implementation JSONConverter

+(NSArray *)toArray:(id)JSON andKey:(NSString *)key{
    NSError *error = nil;
    id object = [NSJSONSerialization JSONObjectWithData:JSON options:0 error:&error];
    
    if ([object isKindOfClass:[NSDictionary class]]){
        NSDictionary *results = object;
        NSArray *resultArray = results[key];
        return resultArray;
    } else {
        return nil;
    }
}
@end
