//
//  ShowAlert.m
//  Brastlewark
//
//  Created by Aleix Cos Pous on 6/2/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

#import "ShowAlert.h"

@implementation ShowAlert
+(void)alertWithTitle:(NSString *)title message:(NSString *)message toViewController:(UIViewController*)viewController{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:action];
    [viewController presentViewController:alert animated:YES completion:nil];
}
@end
