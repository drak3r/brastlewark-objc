//
//  ShowAlert.h
//  Brastlewark
//
//  Created by Aleix Cos Pous on 6/2/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ShowAlert : NSObject
+(void)alertWithTitle:(nullable NSString*)title message:(nullable NSString*)message toViewController:( UIViewController* _Nonnull)viewController;
@end
