//
//  NetworkOperation.m
//  Brastlewark
//
//  Created by Aleix Cos Pous on 4/2/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

#import "NetworkOperation.h"
#import "AFNetworking.h"
#import "JSONConverter.h"
#import "CoreDataManager.h"

@implementation NetworkOperation

+(void)requestGnomesWithResponseHandler:(void (^)(NSArray*, NSError*))handler{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSURL *URL = [NSURL URLWithString:@"https://raw.githubusercontent.com/rrafols/mobile_test/master/data.json"];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        if (error){
            handler(nil, error);
        } else {
            NSArray *JSONArray = [JSONConverter toArray:responseObject andKey:@"Brastlewark"];
            __block NSError *generalError = nil;
            for (NSDictionary* gnomeDict in JSONArray){
                [[CoreDataManager sharedManager] saveGnomeFromDict:gnomeDict completionHandler:^(NSError *error) {
                    if(error){
                        generalError = error;
                    }
                }];
            }
            if(generalError){
                handler(nil, generalError);
            }else {
                [[CoreDataManager sharedManager] fetchGnomesWithCompletionHandler:^(NSArray *gnomes, NSError *error) {
                    if(error){
                        handler(nil,error);
                    } else {
                        handler(gnomes,nil);
                    }
                }];
            }
        }
    }];
    [dataTask resume];
}
@end
