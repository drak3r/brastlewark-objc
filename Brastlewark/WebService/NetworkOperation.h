//
//  NetworkOperation.h
//  Brastlewark
//
//  Created by Aleix Cos Pous on 4/2/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkOperation : NSObject
+(void)requestGnomesWithResponseHandler:(void (^)(NSArray* gnomes, NSError* error))handler;
@end
