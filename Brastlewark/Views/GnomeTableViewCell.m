//
//  GnomeTableViewCell.m
//  Brastlewark
//
//  Created by Aleix Cos Pous on 4/2/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

#import "GnomeTableViewCell.h"

@implementation GnomeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self layoutIfNeeded];
    _imageGnome.layer.cornerRadius = _imageGnome.bounds.size.width/2.0;
    _imageGnome.layer.masksToBounds = YES;
    _viewHairColor.layer.cornerRadius = _viewHairColor.bounds.size.width/2.0;
    _viewHairColor.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
