//
//  ProfessionTableViewCell.h
//  Brastlewark
//
//  Created by Aleix Cos Pous on 4/2/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfessionTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *checkIcon;
@property (weak, nonatomic) IBOutlet UILabel *labelProfession;

@end
