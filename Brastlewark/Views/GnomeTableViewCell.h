//
//  GnomeTableViewCell.h
//  Brastlewark
//
//  Created by Aleix Cos Pous on 4/2/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GnomeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *viewHairColor;
@property (weak, nonatomic) IBOutlet UIImageView *imageGnome;
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UILabel *labelAge;
@property (weak, nonatomic) IBOutlet UILabel *labelProfession;

@end
