//
//  GnomeDetailCollectionViewCell.h
//  Brastlewark
//
//  Created by Aleix Cos Pous on 5/2/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GnomeDetailCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *gnomeImageView;
@property (weak, nonatomic) IBOutlet UILabel *labelName;

@end
