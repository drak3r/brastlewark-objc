//
//  GnomeDetailCollectionViewCell.m
//  Brastlewark
//
//  Created by Aleix Cos Pous on 5/2/17.
//  Copyright © 2017 Aleix Cos Pous. All rights reserved.
//

#import "GnomeDetailCollectionViewCell.h"

@implementation GnomeDetailCollectionViewCell

-(void)awakeFromNib{
    [super awakeFromNib];
    [self layoutIfNeeded];
    _gnomeImageView.layer.cornerRadius = _gnomeImageView.bounds.size.width/2.0;
    _gnomeImageView.layer.masksToBounds = YES;
}
@end
